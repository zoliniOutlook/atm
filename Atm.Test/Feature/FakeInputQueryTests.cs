using System.Threading;
using System.Threading.Tasks;
using Atm.Feature.FakeInput;
using FluentAssertions;
using NUnit.Framework;

namespace Atm.Test.Feature
{
    public class FakeInputQueryTests : TestBase
    {
        // TODO: Add more tests to test different cases for different input files

        [Test]
        public async Task When_MockInput_Is_Equal_To_ExerciseExample_InputModelResult_Is_Built_Correctly()
        {
            var result = await GetHandler().Handle(new FakeInputQuery("Feature/MockInputs/Input.txt"), CancellationToken.None);

            result.AssertRight(model =>
            {
                model.Should().BeEquivalentTo(
                    InputModel_Example_From_Exercise(),
                    "MockInputs/Input.txt is equal to example from exercise");
            });
        }

        private FakeInputQueryHandler GetHandler()
        {
            return new FakeInputQueryHandler();
        }
    }
}