using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Atm.Feature.Session;
using Atm.Infrastructure;
using FluentAssertions;
using MediatR;
using NSubstitute;
using NUnit.Framework;

namespace Atm.Test.Feature
{
    public class SessionCommandTests : TestBase
    {
        // TODO: Add more tests to cover different scenarios

        [SetUp]
        public void SetupAsync()
        {
        }

        [Test]
        public async Task When_InputModel_Example_From_Exercise()
        {
            var inputModel = InputModel_Example_From_Exercise();

            var result = await GetHandler().Handle(new SessionCommand(inputModel), CancellationToken.None);

            result.Result.Should().BeEquivalentTo(
                new List<string>()
                {
                    "500",
                    "400",
                    "90",
                    Constants.Errors.UserHasNoFunds,
                    "0"
                });
        }

        [Test]
        public async Task When_InputModel_Atm_Not_Enough_Balance()
        {
            var inputModel = InputModel_Atm_Not_Enough_Balance();

            var result = await GetHandler().Handle(new SessionCommand(inputModel), CancellationToken.None);

            result.Result.Should().BeEquivalentTo(
                new List<string>()
                {
                    "500",
                    Constants.Errors.AtmOutOfCash,
                    "500",
                });
        }

        [Test]
        public async Task When_InputModel_User_Not_Enough_Balance()
        {
            var inputModel = InputModel_User_Not_Enough_Balance();

            var result = await GetHandler().Handle(new SessionCommand(inputModel), CancellationToken.None);

            result.Result.Should().BeEquivalentTo(
                new List<string>()
                {
                    "500",
                    Constants.Errors.UserHasNoFunds,
                });
        }

        [Test]
        public async Task When_InputModel_User_Uses_Overdraft()
        {
            var inputModel = InputModel_User_Uses_Overdraft();

            var result = await GetHandler().Handle(new SessionCommand(inputModel), CancellationToken.None);

            result.Result.Should().BeEquivalentTo(
                new List<string>()
                {
                    "500",
                    "-100",
                });
        }

        [Test]
        public async Task When_InputModel_Has_No_Sessions()
        {
            var inputModel = InputModel_Has_No_Sessions();

            var result = await GetHandler().Handle(new SessionCommand(inputModel), CancellationToken.None);

            result.Result.Should().BeEquivalentTo(new List<string>());
        }

        private SessionCommandHandler GetHandler()
        {
            return new SessionCommandHandler();
        }
    }
}