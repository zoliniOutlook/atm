﻿using System;
using System.Collections.Generic;
using System.Text;
using Atm.Models;

namespace Atm.Test
{
    public abstract class TestBase
    {
        protected static InputModel InputModel_Example_From_Exercise()
        {
            var inputModel = new InputModel()
            {
                AtmBalance = 8000,
                UsersSessions = new List<UserSession>()
                {
                    new UserSession()
                    {
                        AccountNumber = "12345678",
                        Pin = "1234",
                        PinProvided = "1234",
                        Balance = 500,
                        Overdraft = 100,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction() {Type = TransactionType.Balance},
                            new Transaction() {Type = TransactionType.Withdraw, Amount = 100}
                        }
                    },
                    new UserSession()
                    {
                        AccountNumber = "87654321",
                        Pin = "4321",
                        PinProvided = "4321",
                        Balance = 100,
                        Overdraft = 0,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction() {Type = TransactionType.Withdraw, Amount = 10}
                        }
                    },
                    new UserSession()
                    {
                        AccountNumber = "87654321",
                        Pin = "4321",
                        PinProvided = "4321",
                        Balance = 0,
                        Overdraft = 0,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction() {Type = TransactionType.Withdraw, Amount = 10},
                            new Transaction() {Type = TransactionType.Balance}

                        }
                    },
                }
            };
            return inputModel;
        }

        protected static InputModel InputModel_Atm_Not_Enough_Balance()
        {
            var inputModel = new InputModel()
            {
                AtmBalance = 400,
                UsersSessions = new List<UserSession>()
                {
                    new UserSession()
                    {
                        AccountNumber = "12345678",
                        Pin = "1234",
                        PinProvided = "1234",
                        Balance = 500,
                        Overdraft = 100,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction() {Type = TransactionType.Balance},
                            new Transaction() {Type = TransactionType.Withdraw, Amount = 500},
                            new Transaction() {Type = TransactionType.Balance},

                        }
                    }
                }
            };
            return inputModel;
        }

        protected static InputModel InputModel_User_Not_Enough_Balance()
        {
            var inputModel = new InputModel()
            {
                AtmBalance = 8000,
                UsersSessions = new List<UserSession>()
                {
                    new UserSession()
                    {
                        AccountNumber = "12345678",
                        Pin = "1234",
                        PinProvided = "1234",
                        Balance = 500,
                        Overdraft = 100,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction() {Type = TransactionType.Balance},
                            new Transaction() {Type = TransactionType.Withdraw, Amount = 700},

                        }
                    }
                }
            };
            return inputModel;
        }

        protected static InputModel InputModel_User_Uses_Overdraft()
        {
            var inputModel = new InputModel()
            {
                AtmBalance = 8000,
                UsersSessions = new List<UserSession>()
                {
                    new UserSession()
                    {
                        AccountNumber = "12345678",
                        Pin = "1234",
                        PinProvided = "1234",
                        Balance = 500,
                        Overdraft = 100,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction() {Type = TransactionType.Balance},
                            new Transaction() {Type = TransactionType.Withdraw, Amount = 600},

                        }
                    }
                }
            };
            return inputModel;
        }

        protected static InputModel InputModel_Has_No_Sessions()
        {
            var inputModel = new InputModel()
            {
                AtmBalance = 8000,
            };
            return inputModel;
        }

    }
}
