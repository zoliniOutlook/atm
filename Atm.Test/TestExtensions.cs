﻿using System;
using Atm.Infrastructure;
using LanguageExt;
using NUnit.Framework;

namespace Atm.Test
{
    public static class TestExtensions
    {
        public static void AssertRight<T>(this Either<Error, T> either, Action<T> assertion)
        {
            either.Match(
                Left: error =>
                {
                    if (error.Exception != null)
                        throw error.Exception;
                    Fail($"unexpected left: {error.GetType().Name} - {error.Code}");
                },
                Right: assertion);
        }
        public static void AssertLeft<T>(this Either<Error, T> either, Action<Error> assertion)
        {
            either.Match(
                Left: assertion,
                Right: _ => Fail("unexpected right: " + either));
        }

        private static void Fail(string message)
        {
            Assert.True(false, message);
        }
    }
}
