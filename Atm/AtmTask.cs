﻿using System;
using System.Threading.Tasks;
using Atm.Feature.FakeInput;
using Atm.Feature.Session;
using Atm.Infrastructure;
using Atm.Models;
using LanguageExt;
using MediatR;
using Unit = LanguageExt.Unit;

namespace Atm
{
    public class AtmTask
    {
        private readonly IMediator _mediator;

        public AtmTask(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Either<Error, Unit>> Run()
        {
            // happy path (right side of Either) or return error 
            // info: MapAsync substitutes the right side of Either
            var result = await GetFakeInput()     // returns Task<Either<Error, InputModel>>
                .MapAsync(SessionCommand)         // Task<Either<Error, InputModel>> becomes Task<Either<Error, OutputModel>>
                .MapAsync(WriteOutputToConsole);  // Task<Either<Error, OutputModel>> becomes Task<Either<Error, Unit>>

            // if result is error (left side of Either), then write error message  
            result.IfLeft(error => { WriteErrorToConsole(error); });

            return result;

            async Task<Either<Error, InputModel>> GetFakeInput()
            {            
                return await _mediator.Send(new FakeInputQuery(@"Feature/FakeInput/Input.txt"));
            }
            
            // always return an OutputModel and not an error so we can see which sessions were processed
            // an error in one session does not mean the other session failed 
            async Task<OutputModel> SessionCommand(InputModel model)
            {
                return await _mediator.Send(new SessionCommand(model));
            }

            Unit WriteOutputToConsole(OutputModel output)
            {
                foreach (var r in output.Result)
                {
                    Console.WriteLine(r);
                }

                return Unit.Default;
            }

            void WriteErrorToConsole(Error error)
            {
                Console.WriteLine(error.Code);
            }
        }
    }
}
