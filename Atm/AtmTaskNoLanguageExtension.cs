﻿using System;
using System.Threading.Tasks;
using Atm.Feature.FakeInput;
using Atm.Feature.Session;
using Atm.Infrastructure;
using MediatR;

namespace Atm
{
    public class AtmTaskNoLanguageExtension
    {
        private readonly IMediator _mediator;

        public AtmTaskNoLanguageExtension(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task Run()
        {
            var inputModel = await _mediator.Send(new FakeInputQueryNoLanguageExtension(@"Feature/FakeInput/Input.txt"));
            if (inputModel != null)
            {
                // always return an OutputModel and not an error so we can see which sessions were processed
                // an error in one session does not mean the other session failed 
                var outputModel = await _mediator.Send(new SessionCommand(inputModel));
                foreach (var result in outputModel.Result)
                {
                    Console.WriteLine(result);
                }
            }
            else
            {
                Console.WriteLine(Constants.Errors.InputException);
            }
        }
    }
}
