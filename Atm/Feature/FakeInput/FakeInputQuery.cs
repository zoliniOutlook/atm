﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Atm.Infrastructure;
using Atm.Models;
using LanguageExt;
using MediatR;

namespace Atm.Feature.FakeInput
{
    public class FakeInputQuery : IRequest<Either<Error, InputModel>>
    {
        public string FilePath { get; }

        public FakeInputQuery(string filePath)
        {
            FilePath = filePath;
        }
    }

    public class FakeInputQueryHandler : IRequestHandler<FakeInputQuery, Either<Error, InputModel>>
    {
        private const string SessionSeparator = "|";

        public async Task<Either<Error, InputModel>> Handle(FakeInputQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var inputModel = new InputModel();

                // 1 - read the input file into a list of strings
                var fileLines = await File.ReadAllLinesAsync(request.FilePath, cancellationToken);

                // 2 - substitute empty list item with SessionSeparator as they mark the beginning and end of the session
                var fileLinesWithSeparator = fileLines.Select(line => line == string.Empty ? SessionSeparator : line);

                // 3 - join all the items with empty space separator into one string
                var fileTextWithSeparator = string.Join(" ", fileLinesWithSeparator);

                // 4 - split string by SessionSeparator
                var fileInputFormatted = fileTextWithSeparator.Split(SessionSeparator);

                // At this point the Balance is the first item of the list and the UserSession details are separate items

                // 5 - get balance and add to inputModel
                inputModel.AtmBalance = int.Parse(fileInputFormatted.FirstOrDefault());

                // 6 - for each session we populate the correct fields for inputModel.Sessions  
                foreach (var s in fileInputFormatted.Skip(1))
                {
                    var sessionItems = s.Split(" ");

                    // 7 - session item from 1 to 5 is known based on the convention agreed
                    var session = new UserSession
                    {
                        AccountNumber = sessionItems[1],
                        Pin = sessionItems[2],
                        PinProvided = sessionItems[3],
                        Balance = int.Parse(sessionItems[4]),
                        Overdraft = int.Parse(sessionItems[5])
                    };

                    // 8 - each session item after this point is part of a transaction, so for each transaction item
                    foreach (var transaction in sessionItems.Skip(5))
                    {
                        switch (transaction)
                        {
                            // 9 - if transaction is B, then it is not followed by an amount so we add it to the input model
                            case "B":
                                var balanceTransaction = new Transaction()
                                {
                                    Type = TransactionType.Balance
                                };
                                session.Transactions.Add(balanceTransaction);
                                break;

                            // 10 - if transaction is W, then it is followed by an amount, so we get it and add it to the input model
                            case "W":
                                var withdrawTransaction = new Transaction()
                                {
                                    Type = TransactionType.Withdraw,
                                    Amount = int.Parse(sessionItems[sessionItems.ToList().IndexOf(transaction) + 1])
                                };
                                session.Transactions.Add(withdrawTransaction);
                                break;
                        }
                    }
                    inputModel.UsersSessions.Add(session);
                }
                return inputModel;
            }
            catch (Exception ex)
            {
                // TODO : add logs
                return new Error(Constants.Errors.InputException, ex);
            }
        }
    }
}
