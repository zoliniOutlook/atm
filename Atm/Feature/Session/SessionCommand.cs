﻿using System.Threading;
using System.Threading.Tasks;
using Atm.Infrastructure;
using Atm.Models;
using LanguageExt;
using MediatR;

namespace Atm.Feature.Session
{
    public class SessionCommand : IRequest<OutputModel>
    {
        public InputModel InputModel { get; }

        public SessionCommand(InputModel inputModel)
        {
            InputModel = inputModel;
        }
    }

    public class SessionCommandHandler : IRequestHandler<SessionCommand, OutputModel>
    {
        private int _atmBalance;
        public async Task<OutputModel> Handle(SessionCommand request, CancellationToken cancellationToken)
        {
            var outputModel = new OutputModel();

            try
            {
                var inputModel = request.InputModel;
                _atmBalance = request.InputModel.AtmBalance;

                // for each session
                foreach (var session in inputModel.UsersSessions)
                {
                    // check if pin is valid
                    if (session.Pin != session.PinProvided)
                    {
                        outputModel.Result.Add(Constants.Errors.AuthenticationFailed);
                        continue;
                    }

                    // check if there are transactions
                    if (session.Transactions.IsNull())
                        continue;

                    // for each transaction ( so we can output in order )
                    foreach (var transaction in session.Transactions)
                    {
                        switch (transaction.Type)
                        {
                            // when Balance 
                            case TransactionType.Balance:
                                outputModel.Result.Add(session.Balance.ToString());
                                break;

                            // when Withdraw is bigger than User's Balance + Overdraft
                            case TransactionType.Withdraw when session.Balance - transaction.Amount + session.Overdraft < 0:
                                outputModel.Result.Add(Constants.Errors.UserHasNoFunds);
                                break;

                            // when Withdraw 
                            case TransactionType.Withdraw:

                                // check if ATM has enough Balance
                                if (_atmBalance - transaction.Amount < 0)
                                {
                                    outputModel.Result.Add(Constants.Errors.AtmOutOfCash);
                                    continue;
                                }

                                _atmBalance -= transaction.Amount;
                                session.Balance -= transaction.Amount;

                                outputModel.Result.Add(session.Balance.ToString());
                                break;
                        }
                    }
                }

                await Task.CompletedTask;

                return outputModel;
            }
            catch 
            {
                // TODO : add logs
                outputModel.Result.Add(Constants.Errors.ApplicationException);
                return outputModel;
            }
        }
    }
}
