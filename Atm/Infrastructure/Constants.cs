﻿namespace Atm.Infrastructure
{
    public static class Constants
    {
        public static class Errors
        {
            public static string AuthenticationFailed = "ACCOUNT_ERR";
            public static string AtmOutOfCash = "ATM_ERR";
            public static string UserHasNoFunds = "FUNDS_ERR";
            public static string ApplicationException = "APPLICATION_ERR";
            public static string InputException = "INPUT_ERR";
            
        }
    }
}
