﻿using System;

namespace Atm.Infrastructure
{
    public class Error
    {
        public string Code { get; set; }
        public Exception Exception { get; set; }

        public Error(string code, Exception exception)
        {
            Code = code;
            Exception = exception;
        }
    }
}
