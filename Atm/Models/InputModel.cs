﻿using System.Collections.Generic;

namespace Atm.Models
{
    public class InputModel
    {
        public int AtmBalance { get; set; }
        public List<UserSession> UsersSessions { get; set; } = new List<UserSession>();
    }

    public class UserSession
    {
        public string AccountNumber { get; set; }
        public string Pin { get; set; }
        public string PinProvided { get; set; }

        public int Balance { get; set; }
        public int Overdraft { get; set; }

        public List<Transaction> Transactions { get; set; } = new List<Transaction>();
    }

    public class Transaction
    {
        public TransactionType Type { get; set; }
        public int Amount { get; set; }
    }

    public enum TransactionType
    {
        Balance,
        Withdraw
    }
}
