﻿using System.Collections.Generic;

namespace Atm.Models
{
    public class OutputModel
    {
        public List<string> Result { get; set; } = new List<string>();
    }
}
