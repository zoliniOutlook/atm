﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Atm
{
    internal class Program
    {
        // Program is where we configure the application and run the AtmTask
        public static async Task Main(string[] args)
        {
            // create service collection
            var services = new ServiceCollection();

            // Configure Services
            services.AddMediatR();
            services.AddTransient<AtmTask>();
            services.AddTransient<AtmTaskNoLanguageExtension>();

            // Create service provider
            var serviceProvider = services.BuildServiceProvider();

            // Run the AtmTask.
            await serviceProvider.GetService<AtmTask>().Run();
            
            // TODO: comment previous line and uncomment next line to run the version without using Language.Extension
            //await serviceProvider.GetService<AtmTaskNoLanguageExtension>().Run();
        }
    }
}

