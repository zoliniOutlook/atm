# Intro

This is a C# .NET Core 2.2 Console Application built using Visual Studio.

##### About the code:
* Just for the fun of it, Language Extensions library was used, but an example without it was also added.
See **Relevant information** section bellow for more information about Language Extensions library.
* Program.cs is where the application is configured. This is where you can find information about running the application without Language Extension.
* The code has a lot of comments..



# How to build the code

##### Dependencies
Dependencies are managed via NuGet.
Starting with .NET Core 2.0, `dotnet restore` is run implicitly if necessary when you run `dotnet build`, `dotnet test`, `dotnet publish` and other commands. 
That said, the project can be built from Visual Studio or running `dotnet build` from the root of the project in your command shell.
##### Publish the application 
To generate an exe, you have to generate a self-contained application.
To generate a self-contained application, you must specify the target runtime when publishing the application (which OS you target).
A list of Runtime IDentifiers can be found here https://docs.microsoft.com/en-us/dotnet/core/rid-catalog

##### Windows 10 Release and Debug publishing example
From the root of the project in your command shell run:
```sh
dotnet publish -c Release -r win10-x64
dotnet publish -c Debug -r win10-x64
```
# How to run the output
##### Windows 10 Release and Debug running example
After publishing the application, go to the root of the published targeted runtime
```My root example: C:\Projects\Atm\Atm\bin\Release\netcoreapp2.2\win10-x64```

The project runs with a FakeInput data that comes from a txt file ( Input.txt ).  

* Before publising you can change that file in Visual Studio. 
* After publising you can change that file at the root of the published targeted runtime. 
```My file path example: C:\Projects\Atm\Atm\bin\Release\netcoreapp2.2\win10-x64\Feature\FakeInput\Input.txt```

From the root of the published targeted runtime in your command shell run:

```sh
.\Atm.exe 
```

# How to run tests 
The Atm.Test project can be run from Visual Studio (right click on Atm.Test --> Run Unit Test ) or running `dotnet test` from the root of the project in your command shell.


# Relevant information

This project uses a number of open source projects:

* [Language Extensions](https://github.com/louthy/language-ext) - This library uses the features of C# to provide a functional-programming 'Base class library' with the idea of making programming in C# much more reliable and to make the engineer's inertia flow in the direction of declarative and functional code rather than imperative.
* [MediatR](https://github.com/jbogard/MediatR) -Simple mediator implementation in .NET that allows in-process messaging with no dependencies. 
Supports request/response, commands, queries, notifications and events, synchronous and async with intelligent dispatching via C# generic variance.
* [MediatR.Extensions.Microsoft.DependencyInjection](https://github.com/jbogard/MediatR.Extensions.Microsoft.DependencyInjection) - Scans assemblies and adds handlers, preprocessors, and postprocessors implementations to the container. 

To help with testing:

* [NUnit](https://nunit.org/) -NUnit is a unit-testing framework for all .Net languages



